<div class="sidebar" data-color="black" data-image="assets/img/sidebar-5.jpg">
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="#" class="simple-text">
                            <img src="images/logo.png" width="70%"/>
                        </a>
                    </div>
                    <ul class="nav">
                        <li class="active">
                            <a href="?page=play">
                                <i class="pe-7s-science"></i>
                                <p id="play">Jouer</p>
                            </a>
                        </li>
                        
                        <li>
                            <a href="?page=history">
                                <i class="pe-7s-notebook"></i>
                                <p>Histoire</p>
                            </a>
                        </li>

                         <li>
                            <a href="?page=tools">
                                <i class="pe-7s-tools"></i>
                                <p>Fonctionnement</p>
                            </a>
                        </li>

                         <li>
                            <a href="?page=team">
                                <i class="pe-7s-users"></i>
                                <p>Équipe</p>
                            </a>
                        </li>
                        <div class="logo"></div><br><center>
                                  Points Économiques :
                        <div class="progress progress-striped active" style="width:75%">
                            <div class="progress-bar progress-bar-danger" role="progressbar"
                                aria-valuenow="50" id="progresseco" aria-valuemin="0" aria-valuemax="100" style="width:50%;">
                                50
                            </div>
                        </div>
                                 Points Environnement :
                                   <div class="progress progress-striped active" style="width:75%">
                            <div class="progress-bar progress-bar-success" role="progressbar"
                                aria-valuenow="50" aria-valuemin="0" id="progressenv" aria-valuemax="100" style="width:50%">
                                50
                            </div>
                        </div>
                        </center>
                        <div class="logo"></div><br><center>
<button type="button" class="btn btn-fill">
     <img src="images/fr" id="fr" width="32" /> 
</button><button type="button" class="btn btn-fill">
     <img src="images/en" id="en" width="32" /> 
</button>
                      
                        <li>
                        </ul>

                    </div>
                </div>