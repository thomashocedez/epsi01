

	//Les questions
	question[1] = " Vos employés vous informe la présence de gaz de schiste dans votre ville , souhaitez vous exploitez cette ressource ? ";
	reponse[1] = new Array("Oui","Non","Je ne sais pas");
	score[1] = new Array(5,-3,1);
	
	
	question[2] = "Lors d'une réunion avec le conseil, l'idée d'utiliser des énérgies renouvellable / durable est au sujet , que souhaitez vous faire ?";
	reponse[2] = new Array("Les utiliser", "Ne pas les utiliser ","Ne pas se prononcer");
	score[2] = new Array(3,-5,0);
	

	question[3]= "Une multinational vous contacte car il souhaiterait s'installer dans votre ville , mais pour ce faire , vous devez détruire plusieur hectar de foret , que faites vous ?";
	reponse[3]= new Array("Vous accepter","Vous refusez","Vous n'etes pas sur");
	score [3]= new Array (5,-3,0)
	
	
	question[4]= "On vous previent que la centrale nucléaire de la ville a besoin de rénovation pour etre plus écologique, que souhaitez vous faire ? ";
	reponse[4]= new Array("Vous rénovez","Vous laissez la central en l'état","Vous ferez part de ce communiqué au conseil municipal ");
	score [4]= new Array (5,-3,0)
	

	question[5]="Le data center que votre ville héberge depuis 5ans génère énormement de pollution dans l'air , on vous demande d'agir";
	reponse [5]= new Array("Vous arreter les data center","Vous augmenter la taille du data center","Vous trouvez une alternative ecologique");
	score [5]= new Array (5,-5,3)
	

	question[6]="Suite a une discussion, le président de la Chine souhaite ouvrir une usine chez vous , vous savez pertinemment que cette usine sera très polluante , quel décision prenez vous ? ";
	question[6]="After a discussion, the president of China wants to open a factory at home, you know full well that this plant will be very polluting, what decision do you take?"
	reponse[6]= new Array ("Vous donnez votre accord","Vous refusez","Vous n'etes pas sur");
	score[6]=new Array (-7,5,0)
	

	question[7]="Vous apprenez qu'il y a une hausse des sans logement dans votre ville, que faites vous ?";
	question[7]="You learn that there is a rise in homelessness in your city, what do you do?"
	reponse[7]= new Array ("Vous construisez un centre social","Vous ignorez cette avertissement","Vous ne savez pas quoi faire");
	score[7]= new Array (5,-5,1)
	

	question[8]="On vous demande de faire une circulation alterneé pour les véhicules polluants, acceptez-vous? ";
	question[8]="You are asked to make a circulation alternate for polluting vehicles, do you agree ?"
	reponse[8]= new Array ("Oui","Non","Ne pas se prononcer");
	score[8]= new Array (5,-3,1)
	


	question[9]=" Souhaitez-vous, aménager un parc pour la création d'un club de golf privée ? ";
	question[9]="Would you like to set up a park for the creation of a private golf club?"
	reponse[9]= new Array ("Donner son accord","Refuser","Ne pas se prononcer");
	score[9]= new Array (-5,5,0)
	

	question[10]="Voulez vous aider financièrement un groupe de défense des animaux";
	question[10]="Would you like to financially support an animal welfare group?"
	reponse[10]= new Array ("Oui","Non","Pourquoi faire ?");
	score[10]= new Array (5,-5,-1)
	
	
	question[11]="Votre commune a recu une proposition pour crée une décharge pour d'autres communes, mais cela vous coûtera une partie de votre territoire ";
	question[11]="Your commune has received a proposal to create a landfill for other municipalities, but it will cost you a part of your territory?"
	reponse[11]= new Array ("Oui","Non","Ne pas se prononcer");
	score[11]= new Array (4,-3,1)
	

	
	question[12]="Aménager une route pour augmenter l'accès à votre ville, qui passe par la forêt ?";
	question[12]="Set up a road to increase access to your town, which passes through the forest?"
	reponse[12]= new Array ("Pourquoi pas","Hors de question","C'est envisageable sous certaine condition ");
	score[12]= new Array (-3,4,-1)
		


	question[13]="Vous evaluez vos B1 sur ce jeu , quel note allez vous leur donner ? ";
	question[13]="You rate your B1 on this game, what rating did you give them?"
	reponse[13]= new Array ("20","20","20");
	score[13]= new Array (5,5,5)
	

