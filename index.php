<?php
session_start();
require_once('template/doctype.php');
require_once('template/header.php');

if(!isset($_GET['page'])){$_GET['page'] = "tools";}
$page = $_GET['page'];


require_once('template/menu.php');
?>

<div class="main-panel">
<div class="content">
       <div class="container-fluid">
<?php

if(file_exists('core/'.$page.'.php')){
	include('core/'.$page.'.php');

}else{
	include('core/tools.php');
}
?>
</div></div>
<?php
require_once('template/footer.php');
?>
</div>




