# Polus

Game project for national 2017 EPSI workshop


## Links:

## Quick start

Quick start options:

- Clone the repo: `git clone git@framagit.org:thomashocedez/epsi01.git`.


### What's included

Within the download you'll find the following directories and files:


### Version logs


V0.1 - March 2017 [current version]
- switched to MIT license

### License

- Licensed under MIT (https://framagit.org/thomashocedez/epsi01/blob/master/LICENSE.md)

## Useful Links
